// ==UserScript==
// @name         otrs lite
// @description  Функционал/стили OTRS
// @namespace    otrs-lite-masterwind
// @author       masterwind
// @version      21.7.30
// @updateURL    https://otrslite.netlify.app/otrslite.user.js
// @downloadURL  https://otrslite.netlify.app/otrslite.user.js
// @supportURL   https://bitbucket.org/masterwind/otrslite/issues/
// @include      http://sp.ucoz.ru/otrs/*

// @require      https://code.jquery.com/jquery-2.2.4.min.js
// @require      https://code.jquery.com/ui/1.12.1/jquery-ui.min.js
// @require      https://code.jquery.com/jquery-migrate-1.4.1.min.js

// @require      https://otrslite.netlify.app/lity.min.js
// @icon         https://otrslite.netlify.app/icon.png
// @grant        unsafeWindow
// @noframes
// ==/UserScript==

window = unsafeWindow;

;((stp, app, tpl, CSS = ['lity'] ) => {
stp.debug && console.log('stp', stp);
stp.debug && console.log('app', app);
stp.debug && console.log('tpl', tpl);

	// init
	document.body.id = 'app';
	app.getPersonalSettings();
	app.initInsertAtCaret();



	// OPEN ALL TICKETS
stp.debug && console.log(
	`pageIs=(AgentTicketQueue | AgentTicketSearch) & FixedTable & pages`,
	app.pageIs('AgentTicketQueue'), app.pageIs('AgentTicketSearch'), !!window.FixedTable, (document.querySelectorAll('#FixedTable tbody tr').length <= stp.limits.openAllTickets)
);
	app.pageAny(['AgentTicketQueue', 'AgentTicketSearch'])
		&& CSS.push('queue')
		&& window.FixedTable
		&& (window.ticketRows = document.querySelectorAll('#FixedTable tbody tr'))
		&& (ticketRows.length <= stp.limits.openAllTickets) // открыть одновременно более 10 вкладок нагрузочно
		&& !document.querySelector('.TicketNumber').insertAdjacentHTML('beforeend', tpl.button.openAllTickets);


	// TOGGLE QUEUES
stp.debug && console.log(`pageIs=AgentTicketQueue`, app.pageIs('AgentTicketQueue') );
	app.pageIs('AgentTicketQueue')
		&& document.querySelectorAll('.newqueue > b').forEach(item => item.addEventListener('click', event => event.target.classList.toggle('hidden')));


	// MASS ACTION
stp.debug && console.log(`pageIs=AgentTicketBulk`, app.pageIs('AgentTicketBulk') );
	app.pageIs('AgentTicketBulk')
		&& CSS.push('action')
		&& CSS.push('massAction')
		&& document.compose && (document.compose.StateID.value = 4) // ticket status is 'opened'
		&& (footer = document.querySelector('.Footer'))
		&& footer.insertAdjacentHTML('beforeend', tpl.button.mergeTickets);


	// TICKET PRIORITY
stp.debug && console.log(`pageIs=AgentTicketPriority`, app.pageIs('AgentTicketPriority') );
	app.pageIs('AgentTicketPriority')
		&& CSS.push('action')
		&& (compose.NewPriorityID.value = 5);


	// TICKET PAGE
stp.debug && console.log(`pageIs=AgentTicketZoom`, app.pageIs('AgentTicketZoom') );
	// init
	app.pageIs('AgentTicketZoom') && CSS.push('ticket');
	// fast move buttons
	app.pageIs('AgentTicketZoom')
		&& window.DestQueueID && [...DestQueueID.options].map(option => !!+option.value && (stp.queues[+option.value] = option.text.trim()))
		&& app.setMoveSetup('fastMove') // set move setup
		&& app.setContainer('fastMove', 'queues', 'allowMoveQueues')
		&& app.renderMoveButtons();
	// fast reply
	app.pageIs('AgentTicketZoom')
		&& window.ResponseID && [...(ResponseID.options || ResponseID[0].options)].map(option => !!+option.value && (stp.copypastes[+option.value] = option.text.trim()))
		&& app.setReplySetup('fastReply') // set reply setup
		&& app.setContainer('fastReply', 'copypastes', 'allowFastReply')
		&& app.renderReplyButtons();
	// create bug to jira - TODO


	// TICKET REPLY
stp.debug && console.log(`pageIs=AgentTicketCompose`, app.pageIs('AgentTicketCompose') );
	// init
	app.pageAny(['AgentTicketCompose', 'AgentTicketForward'])
		&& CSS.push('action', 'ticketReply')
		// && window.ResponseID && [...ResponseID.options].map(option => !!+option.value && (stp.copypastes[+option.value] = option.text.trim()))
	;
	// set copypastes
	app.pageIs('AgentTicketCompose')
		&& app.setCopypasteSetup('copyPaste')
		&& app.setContainer('copyPaste', 'customCopyPaste')
		&& app.renderCopypasteButtons();
	// set custom buttons
	app.pageAny(['AgentTicketCompose', 'AgentTicketForward'])
		&& !submitRichText.insertAdjacentHTML('beforebegin', tpl.button.opened)
		&& !submitRichText.insertAdjacentHTML('afterend', tpl.button.closed);


	// FETCH URGENT TASK
stp.debug && console.log(`window.qurgent`, !!window.qurgent );
	app.pageIs('AgentTicketQueue')
		? window.qurgent && (fetchedUrgentBlock = app.getUrgentBlock()) && window.Logo && Logo.insertAdjacentElement('beforeend', fetchedUrgentBlock)
		: fetch('?Action=AgentTicketQueue')
			.then(response => response.text())
			.then((text,
				domParser = new DOMParser,
				html = domParser.parseFromString(text, 'text/html'),
				fetchedUrgentBlock = app.getUrgentBlock(html),
			) => {
				fetchedUrgentBlock && window.Logo && Logo.insertAdjacentElement('beforeend', fetchedUrgentBlock);
			});


	// USER CUSTOM PREFERENCES
stp.debug && console.log(`pageIs=AgentPreferences`, app.pageIs('AgentPreferences') );
	app.pageIs('AgentPreferences')
		&& CSS.push('preferences')
		&& app.setCustomUserPreferences();


	// SET STYLE
stp.debug && console.log(`CSS`, CSS );
	app.setCSS(tpl.CSS.global.trim() + CSS.map(item => tpl.CSS[item]).join('\n'));
})(
	window.stp = {
		// debug : 1,
		limits : {
			openAllTickets : 10,
		},
		queues : [],
		allowMoveQueues : [],
		copypastes : [],
		allowFastReply : [],
		customCopyPaste : [],

		// additionalQueuesKey : 'customUserPreferences',
		config : {
			customUserPreferences : [
				{
					key : 'additionalQueues',
					title : 'Очереди быстрого доступа',
					description : 'Введите индентификаторы очередей, для отображения в блоке быстрого доступа вверху страницы (через запятую или пробел)',
				},
			],
		},
	},
	window.app = {
		// utils
		setCSS : CSS => window.otrsCSS && (otrsCSS.innerHTML = CSS) || document.head.insertAdjacentHTML('beforeend', `<style id=otrsCSS>${CSS}</style>`),
		pageAny : pages => !!pages.filter(page => location.href.includes(page)).length,
		pageIs : (page, alternate = page.replace(';', '&'), curPage = stp.curPage = page ) => app.pageAny([page, alternate]),
		openAll : hrefArray => hrefArray.forEach(href => window.open(href)),
		openAllTickets : tickets => tickets.forEach(item => window.open(item.querySelector('.MasterActionLink').href)),
		initInsertAtCaret : () => {
			HTMLTextAreaElement.prototype.insertAtCaret = function(text) {
				var start = this.selectionStart;
				var end = this.selectionEnd;
				if (start || start == 0 ) {
					this.value = this.value.substring(0, start) + text + this.value.substring(end, this.value.length);
					this.focus();
					this.selectionStart = start + text.length;
					this.selectionEnd   = start + text.length;
				} else {
					this.value += text;
				}
			};
		},

		// do settings
		synchronousSettings : () => !localStorage.setItem('otrsPersonalStp', JSON.stringify(otrsPersonalStp)) && app.getPersonalSettings() && lity.current() && lity.current().close(),
		saveSettings : (button, form = button.form, ops = otrsPersonalStp[form.settings_block.dataset.key] = {} ) => [...button.form.elements].map(item => item && (item.value
				&& (otrsPersonalStp[form.settings_block.dataset.key][item.value] = +item.checked)
				|| item.value && (delete otrsPersonalStp[form.settings_block.dataset.key][item.value], delete stp[form.settings_block.dataset.key][item.value]))
			)
			&& app.synchronousSettings()
			&& (app.renderMoveButtons(), app.renderReplyButtons()),
		saveData : (button, form = button.form, fields = form.settings_block ) => {
			(otrsPersonalStp[fields.dataset.key] = [...fields.children]
				.filter(item => item.children.key.value && item.children.text.value)
				.map(item => [item.children.key.value.trim(), item.children.text.value.trim()]))
			&& app.synchronousSettings()
			&& app.renderCopypasteButtons()
		},
		savePreferences : (button, form = button.form, ops = otrsPersonalStp[form.settings_block.dataset.key] ) => {
			[...button.form.elements].filter(item => !!item.value).map(item => {
				item && (otrsPersonalStp[form.settings_block.dataset.key] = item.value.trim())
			}).length || (delete otrsPersonalStp[form.settings_block.dataset.key], delete stp[form.settings_block.dataset.key]);
			app.synchronousSettings();
		},

		// modal
		modal : {
			render : (name, data, params ) => tpl.get(tpl.modal[name].content, {
				content : stp[data].map((item, idx ) => tpl.get(tpl.modal[name].item, {
					checked : !!stp[params][idx] && 'checked',
					value   : idx,
					name    : item, })).join(''),
				submit  : tpl.modal[name].submit || tpl.button.submit,
			}
			),
			getData : (name, data ) => tpl.get(tpl.modal[name].content, {
				content : stp[data].map((item, idx ) => tpl.get(tpl.modal[name].item, {
						name : item[0],
						text : item[1],
					})).join('') || '<div info>Нет ни одной добавленной копипасты.<br>Нажмите "Добавить" и внесите данные в форму, после чего сохраните данные.</div>',
				submit  : tpl.modal[name].submit || tpl.button.submit,
				add     : tpl.modal[name].add,
			}
			),
		},
		setContainer : (name, data, params ) => !document.body.insertAdjacentHTML('beforeend',
			tpl.get(tpl.modal.container, {id : name, content : params && app.modal.render(name, data, params) || app.modal.getData(name, data) })),

		setMoveSetup : name => window.DestQueueID && !DestQueueID.insertAdjacentHTML('afterend',
			`<a move-setup setup-button data-lity href=#${name} ></a>`),
		renderMoveButtons : (container = document.querySelector('.ActionRow .Clear') ) => container && !container.classList.add('fast-move')
			&& (container.innerHTML = Object.keys(stp.allowMoveQueues).map(queue => tpl.get(tpl.button.moveTicket, {id:queue, name:stp.queues[queue]})).join('')),

		// setPageSetup : name => window.UserInfo && !UserInfo.insertAdjacentHTML('beforeend', `<a page-setup data-lity href=#${name} ></a>`),

		setReplySetup : name => window.ArticleTree && !ArticleTree.insertAdjacentHTML('afterend',
			`<div reply-wrapper><div reply-content></div><a reply-setup setup-button data-lity href=#${name} ></a></div>`),
		renderReplyButtons : (container = document.querySelector('[reply-content]') ) => container
			&& (container.innerHTML = Object.keys(stp.allowFastReply).map(copy => stp.copypastes[copy] && tpl.get(tpl.button.replyTicket, {id:copy, name:stp.copypastes[copy]})).join('')),

		setCopypasteSetup : (name, fieldset = document.compose && compose.querySelector('fieldset') ) => fieldset && !fieldset.insertAdjacentHTML('beforeend',
			`<div copypaste-wrapper><div copypaste-content></div><a copypaste-setup setup-button data-lity href=#${name} ></a></div>`),
		renderCopypasteButtons : (container = document.querySelector('[copypaste-content]')) => container
			&& (container.innerHTML = stp.customCopyPaste.filter(item => item[0] && item[1]).map(item => tpl.get(tpl.button.copyPaste, {name : item[0], text : item[1], })).join(' ')),
		addField : (button, form = button.form, fields = form.settings_block ) => !fields.insertAdjacentHTML('beforeend', tpl.get(tpl.modal.copyPaste.item, {})) && app.removeNotifications(),
		removeNotifications : (elements = document.querySelectorAll('[info], [warning], [alert]') ) => elements && elements.forEach(element => element.remove()),

		// get/set settings
		getPersonalSettings : () => (window.otrsPersonalStp = {})
			&& localStorage.otrsPersonalStp && (otrsPersonalStp = JSON.parse(localStorage.otrsPersonalStp))
			&& !Object.entries(otrsPersonalStp).forEach(item => stp[item[0]] = item[1]),
		moveTicketTo : button => window.DestQueueID && (DestQueueID.querySelector(`option[value="${button.value}"]`).selected = true) && DestQueueID.form.submit(),
		serialize : (form, obj = {},
			tmp = [...new FormData(form)].forEach(entry => entry[1] && (obj[entry[0]] = entry[1]))
		) => form && Object.keys(obj).map(key => `${key}=${obj[key]}`).join('&'),
		doReply : (button,
			all = document.querySelectorAll('#ResponseID'),
			last = all[all.length - 1],
			url = (last.value = button.value) && Core.Config.Get('Baselink') + app.serialize(last.form)
		) => Core.UI.Popup.OpenPopup(url, 'TicketAction') || (button.value = 0),
		toggleField : element => element.classList.toggle('hide-field'),
		// custom user preferences
		setCustomUserPreferences : () => {
			stp.config.customUserPreferences.map(pref => {
				pref && (targetForm = document.forms[document.forms.length - 1])
					&& targetForm.insertAdjacentHTML('afterend', tpl.get(tpl.preferences.wrapper, {
						content: tpl.get(
							tpl.preferences.content,
							pref,
							{
								value : otrsPersonalStp[pref.key] || '',
								submit : tpl.preferences.submit || tpl.button.submit,
							})
					}))
			});
		},

		// urgent block
		getUrgentBlock : (source = document) => {
			// get default urgent block
			urgentBlock = source.documentElement.querySelector('#qurgent');
			// get additional links
			additionalQueuesIDs = stp.additionalQueues && stp.additionalQueues.split(/[\s,|/\-:;]+/);
			additionalQueuesList = additionalQueuesIDs && source.documentElement.querySelectorAll(additionalQueuesIDs.map(id => `a[href*="QueueID=${id};"]`).join(','));
			// append this links
			urgentBlock && (targetList = urgentBlock.querySelector('ul')) && additionalQueuesList && additionalQueuesList.length && additionalQueuesList.forEach(item => {
					((subElement = document.createElement('LI')) && subElement.appendChild(item)) && targetList.appendChild(subElement);
			});

			return urgentBlock;
		},
	},
	window.tpl = {
		get : (tmpl, data, extended = {},
			// prerender template conditions
			condition = /\?\?(.+?)\?\?/.test(tmpl)
				&& (tmpl = tmpl.replace(/\?\?(.+?)=(.+?)\?\?/g, (match, name, template) => data[name] && template || '')),
		) => tmpl.replace(/\{\{(.+?)\}\}/g, (match, group) => data[group] || extended[group] || ''),

		button : {
			openAllTickets : '<button type=button class=open-all-tickets onclick="app.openAllTickets(ticketRows)">Открыть все!</button>',
			mergeTickets : ' <button class="Primary" onclick="compose.MergeToSelection[1].checked = !0" title="Объединить" type="submit" value="Объединить">Объединить</button> ',
			moveTicket : ' <button value="{{id}}" move-to type=button onclick=app.moveTicketTo(this)>{{name}}</button> ',
			replyTicket : ' <button value="{{id}}" reply-to type=button onclick=app.doReply(this)>{{name}}</button> ',
			copyPaste : ' <button use-copypaste type=button onclick="RichText.insertAtCaret(`{{text}}`)">{{name}}</button> ',
			submit : ' <button type=button onclick=app.saveSettings(this)>Сохранить</button> ',
			opened : ' <button class="Primary" onclick="compose.StateID.value = 4" type="submit">Открыт</button> ',
			closed : ' <button class="Primary" onclick="compose.StateID.value = 2" type="submit">Закрыть успешно</button> ',
		},

		modal : {
			container : '<div id={{id}} modal-content class=lity-hide>{{content}}</div>',
			fastMove : {
				item : '<label><input type=checkbox {{checked}} value={{value}}> {{name}}</label>',
				content : `<h1>Настройки очередей</h1>
				<description>Выберите очереди, которые отображать в блоке быстрого перемещения тикета.</description>
				<form><fieldset name=settings_block data-key=allowMoveQueues><section columns>{{content}}</section></fieldset>{{submit}}</form>`,
			},
			fastReply : {
				item : '<label><input type=checkbox {{checked}} value={{value}}> {{name}}</label>',
				content : `<h1>Настройки быстрого ответа</h1>
				<description>Выберите ответы, которые отображать в блоке быстрого ответа на тикет.</description>
				<form><fieldset name=settings_block data-key=allowFastReply><section columns>{{content}}</section></fieldset>{{submit}}</form>`,
			},
			copyPaste : {
				item : '<field copy-paste><toggle onclick=app.toggleField(this)></toggle><input type=text placeholder="Название" name=key value="{{name}}"><textarea placeholder="Содержимое копипасты" name=text>{{text}}</textarea></field>',
				content : `<h1>Настройки кастомных копипаст</h1>
				<description>Добавьте самые часто-используемые копипасты и держите их всегда под рукой.</description>
				<form><fieldset name=settings_block data-key=customCopyPaste>{{content}}</fieldset>{{submit}} {{add}}</form>`,
				submit : '<button type=button onclick=app.saveData(this)>Сохранить</button>',
				add : '<button type=button onclick=app.addField(this) title="Добавить копипасту">Добавить</button>',
			},
		},

		preferences : {
			wrapper : '<form class=Validate onsubmit="return false" id=customUserPreferences>{{content}}</form>',
			content : `<h3>{{title}}</h3>
			<p class=FieldExplanation>{{description}}</p>
			<fieldset class=TableLike name=settings_block data-key={{key}} >
				<div class="FloatingField">
					<input type=text id=fastQueuesIDs value="{{value}}">
					{{submit}}
				</div>
			</fieldset>`,
			submit : ' <button type=button onclick=app.savePreferences(this)>Сохранить</button> ',
		},

		CSS : {
			lity : `
.lity{z-index:9990;position:fixed;top:0;right:0;bottom:0;left:0;white-space:nowrap;background:#0b0b0b;background:rgba(0,0,0,.85);outline:none !important;opacity:0;-webkit-transition:opacity .3s ease;-o-transition:opacity .3s ease;transition:opacity .3s ease}.lity.lity-opened{opacity:1}.lity.lity-closed{opacity:0}.lity *{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.lity-wrap{z-index:9990;position:fixed;top:0;right:0;bottom:0;left:0;text-align:center;outline:none !important}.lity-wrap:before{content:'';display:inline-block;height:100%;vertical-align:middle;margin-right:-0.25em}.lity-loader{z-index:9991;color:#fff;position:absolute;top:50%;margin-top:-0.8em;width:100%;text-align:center;font-size:14px;font-family:Arial,Helvetica,sans-serif;opacity:0;-webkit-transition:opacity .3s ease;-o-transition:opacity .3s ease;transition:opacity .3s ease}.lity-loading .lity-loader{opacity:1}.lity-container{z-index:9992;position:relative;text-align:left;vertical-align:middle;display:inline-block;white-space:normal;max-width:90%;max-height:90%;outline:none !important}.lity-content{z-index:9993;width:100%;-webkit-transform:scale(1);-ms-transform:scale(1);-o-transform:scale(1);transform:scale(1);-webkit-transition:-webkit-transform .3s ease;transition:-webkit-transform .3s ease;-o-transition:-o-transform .3s ease;transition:transform .3s ease;transition:transform .3s ease, -webkit-transform .3s ease, -o-transform .3s ease}.lity-loading .lity-content,.lity-closed .lity-content{-webkit-transform:scale(.8);-ms-transform:scale(.8);-o-transform:scale(.8);transform:scale(.8)}.lity-content:after{content:'';position:absolute;left:0;top:0;bottom:0;display:block;right:0;width:auto;height:auto;z-index:-1;-webkit-box-shadow:0 0 8px rgba(0,0,0,.6);box-shadow:0 0 8px rgba(0,0,0,.6)}.lity-close{z-index:9994;width:35px;height:35px;position:fixed;right:0;top:0;-webkit-appearance:none;cursor:pointer;text-decoration:none;text-align:center;padding:0;color:#fff;font-style:normal;font-size:35px;font-family:Arial,Baskerville,monospace;line-height:35px;text-shadow:0 1px 2px rgba(0,0,0,.6);border:0;background:none;outline:none;-webkit-box-shadow:none;box-shadow:none}.lity-close::-moz-focus-inner{border:0;padding:0}.lity-close:hover,.lity-close:focus,.lity-close:active,.lity-close:visited{text-decoration:none;text-align:center;padding:0;color:#fff;font-style:normal;font-size:35px;font-family:Arial,Baskerville,monospace;line-height:35px;text-shadow:0 1px 2px rgba(0,0,0,.6);border:0;background:none;outline:none;-webkit-box-shadow:none;box-shadow:none}.lity-close:active{top:1px}.lity-image img{max-width:100%;display:block;line-height:0;border:0}.lity-iframe .lity-container,.lity-youtube .lity-container,.lity-vimeo .lity-container,.lity-facebookvideo .lity-container,.lity-googlemaps .lity-container{width:100%;max-width:964px}.lity-iframe-container{width:100%;height:0;padding-top:56.25%;overflow:auto;pointer-events:auto;-webkit-transform:translateZ(0);transform:translateZ(0);-webkit-overflow-scrolling:touch}.lity-iframe-container iframe{position:absolute;display:block;top:0;left:0;width:100%;height:100%;-webkit-box-shadow:0 0 8px rgba(0,0,0,.6);box-shadow:0 0 8px rgba(0,0,0,.6);background:#000}.lity-hide{display:none}
`,
			global : `
/* GLOBAL */
#app #Header, #app #SHeader{height:50px; border:0; }
#app #Navigation{top:17px; }
#SHeader{display:flex; justify-content:flex-end; align-items:center; padding:0 5px; }
#SHeader > * {position:static; margin:0 5px; }
#Logo #qurgent{display:flex; background: url('/otrs-web/skins/Agent/default/img/pattern.jpg'); color:black; padding:3px 9px; border-radius:5px; border:1px dotted #999;
	margin:0; width:auto; }
#Logo #qurgent b{margin:0 10px 0 0; }
#Logo #qurgent ul, #Logo #qurgent li{margin:0; list-style:none; line-height:1.2em; }
a[href*="QueueID=23"], a[href*="QueueID=27"], a[href*="QueueID=36"]{color:red; font-weight:bold; }

#app #UserInfo{display:flex; padding:0; justify-content:flex-end;  align-items: center;}
#app #UserInfo > *{position:static; }
#app #UserInfo > a{margin:0 0 0 5px; }
#app #UserInfo [page-setup]{display:inline-block; width:19px; text-align:center; background:#9a9a9a; text-decoration:none;
	border-radius:2px; padding:1px 1px 0 0; }
#app #UserInfo [page-setup]:hover{background:linear-gradient(#efaf3e, #e36f30); }

[setup-button]{padding:1px 2px; }
[setup-button]:before{content:'\\2699'; font-size:20px; font-weight:bold; color:#1a1a1a; }

#Footer{margin:0!important; padding:3px 7px 0px!important; position:fixed!important; bottom:0!important; width:100%!important;
	background:#74C5E5!important; z-index:100; }
#Footer a{color:black!important; }
#Footer div.TopOfPage {top:4px!important; }

.lity label{display:flex; align-items:center; }
input[type=checkbox]{margin:0 5px 0 0; }

[modal-fade]{position:fixed; top:0; right:0; bottom:0; left:0; background:rgba(2,3,4,.5); z-index:999; display:flex;
	justify-content:flex-end; align-items:flex-start; }
[modal-content]{background:#fff url('/otrs-web/skins/Agent/default/img/pattern.jpg'); padding:20px; border-radius:5px;
	margin:0; overflow:auto; font:normal 1.1em/1.3 sans-serif; }
[modal-content] description{margin:0 0 1em; display:block; }
[modal-content] button{margin:1em 0 0; }
fieldset [columns]{display:grid; grid-gap:0 1em; grid-template-columns:1fr 1fr 1fr; }

[info]{color:darkorange; text-align:center; }
`,
			action : `
/* ACTION */
#app .Header{background:#74C5E5; }
#app .Header h1{margin:0; }
#app .Header p{margin:0; }
#app .Header p a{color:brown; }
#app .LayoutPopup > .Content{margin:0; padding:0 10px 36px; box-sizing:border-box; }
#app .Footer{position:fixed; bottom:0px; width:100%; min-height:24px; background:#74c5e5; padding:4px 0; margin:0; }
`,
			queue : `
/* QUEUE */
.WidgetSimple{padding:0; margin:2px 10px 10px; }
.WidgetSimple .Content{padding:5px; }
.WidgetSimple .newqueue .hidden + ul{display:none; }

#OverviewControl > div{padding:0 10px; box-sizing:border-box; }
#OverviewControl.Fixed > div{position:fixed; top:0; width:100%; }
#OverviewBody{margin:0 10px 20px; }
#OverviewBody.FixedHeader{padding:73px 0 0; }
#OverviewBody.FixedHeader thead tr{margin:0 10px; display:table-row; }
#OverviewBody th, #OverviewBody td{box-sizing:border-box; width:auto; }
#OverviewBody.FixedHeader.Fixed thead tr{margin:0 10px; }

.TicketNumber{position:relative; }
.open-all-tickets{font-size:0.9em; position:absolute; top:0; right:0; }

@media (max-width:1366px ) {
	#OverviewBody{margin:0 0 20px; }
	#OverviewControl > div{padding:0; }
	#OverviewBody.FixedHeader thead tr,
	#OverviewBody.FixedHeader.Fixed thead tr{margin:0; }
}
`,
			massAction : `
/* MASS ACTION */
.MessageBox.Notice{display:inline-block; margin:0; padding:0; }
.MessageBox.Notice p{padding:3px 6px; }
#app .Header h2{display:none; }
`,
			ticket : `
/* TICKET */
#app .LightRow.Header{background:#74C5E5; }
#app .MainBox{padding:0 10px 29px; }

.fast-move{text-align:right; padding:2px; }
[move-to], [reply-to]{font-size:.9em; }

.ActionRow form{display:flex; }
#app [move-setup]{display:inline-block; padding:1px 2px; }
#app [move-setup]:hover{background:none; border-left:1px solid transparent; border-right:1px solid transparent; box-shadow:none; }

[reply-wrapper]{display:flex; justify-content:flex-end; margin:10px 0; }
`,
			ticketReply : `
/* TICKET REPLY */
#app .Footer{box-sizing:border-box; display:flex; padding:4px 8px; justify-content:space-between; }
[copypaste-wrapper]{display:flex; justify-content:flex-end; margin:0; }
field[copy-paste]{display:flex; justify-content:space-between; align-items:flex-start; border-bottom:1px solid #ccc; padding:5px 0; }
field[copy-paste] input{width:30vw; margin: 0 10px 0 0;}
field[copy-paste] textarea{width:50vw; }

[use-copypaste]{font-size:.9em; }

toggle{margin:0 5px 0 0; cursor:cell; }
toggle:before{content:'\\2796'; }
toggle.hide-field:before{content:'\\2795'; }
toggle.hide-field ~ *{display:none; }
`,
			preferences : `
/* USER PREFERENCES */
#customUserPreferences .FloatingField{width:100%; box-sizing:border-box; }
#fastQueuesIDs{width:100%; box-sizing:border-box; }`,
		},
	},
);
